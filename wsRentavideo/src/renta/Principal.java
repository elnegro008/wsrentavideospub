package renta;

import java.util.Scanner;
import java.util.Vector;

public class Principal {

	public static void main(String[] args) {
		Scanner leer =new Scanner(System.in);
		int opcion;
		Datos datos1=new Datos();
		do{
			System.out.println("Elija una opcion: ");
			System.out.println("1-Rentar videos");
			System.out.println("2-Total por tipo");
			System.out.println("3-Total por membresia");
			System.out.println("4-Salir");			
			opcion=leer.nextInt();
			switch(opcion){
			case 1:
				System.out.println("Introduzca codigo del cliente");
				int codigo;
				do{
					codigo=leer.nextInt();
					if(!datos1.existeCliente(codigo)){
						System.out.println("codigo no valido, vuelva a introduzirlo");						
					}
				}while(!datos1.existeCliente(codigo));				
				Vector<Video> vid = datos1.getVideos();				
				System.out.println("Catalogo:");
				System.out.printf("%-10s%-20s%-20s%-20s\n","Id","Nombre","Precio","Tipo");
				for (int i=0; i<vid.size();i++){
					System.out.printf("%-10s%-20s%-20s%-20s\n",vid.elementAt(i).id,vid.elementAt(i).nombre,vid.elementAt(i).precio,vid.elementAt(i).tipo);
					
				}				
				int id;
				int otra;
				Vector<Video> videos_rentados=new Vector<Video>();
				do{
					System.out.println("\nIntroduzca id de video a rentar");
					do{
						id=leer.nextInt();
						if(!datos1.existeVideo(id)){
							System.out.println("id no valido, vuelva a introduzirlo");						
						}
					}while(!datos1.existeVideo(id));
					videos_rentados.addElement(datos1.buscarVideo(id));
					System.out.println("Introduzca (1) para terminar o (2) para agregar otro video");
					otra=leer.nextInt();					
				}while(otra!=1);
				datos1.agregarRenta(new Renta(codigo,videos_rentados));	
				System.out.println("Renta completada");
				break;
			case 2:
				datos1.totalPorCategoria();
				break;
			case 3:
				int idCliente;
				int volverB;
				do{
					System.out.println("Introduzca codigo del cliente para buscar renta");
					idCliente=leer.nextInt();
					if(!datos1.existeRenta(idCliente)){
						System.out.println("Cliente sin renta");
						
					}else{
						System.out.println("Cliente con renta");
						datos1.totalPagarMembrecia(idCliente);
						 
					}
					System.out.println("�Volver a buscar?, 1 s�, 2 no");
					volverB = leer.nextInt();
					
				}while(volverB == 1);				
								
				System.out.println("");

				break;
			case 4:System.out.println("Adios!!");
				break;
			default:System.out.println("Opcion no valida");
			}
		}while(opcion!=4);

	}

}
