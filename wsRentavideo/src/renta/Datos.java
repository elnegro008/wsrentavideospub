package renta;

import java.util.Vector;

public class Datos {
	Vector<Cliente> clientes;
	Vector<Video> videos;
	Vector <Renta> rentas;
	
	public Datos(){
		clientes = new Vector<Cliente>();
		videos = new Vector<Video>();	
		rentas = new Vector<Renta>();
		
		clientes.addElement(new Cliente(001,"David Miranda","normal"));
		clientes.addElement(new Cliente(002,"Cesar Manjarrez","normal"));
		clientes.addElement(new Cliente(003,"Manuel Lugo","gold"));
		clientes.addElement(new Cliente(004,"Aaron Velazquez","gold"));
		clientes.addElement(new Cliente(005,"Mario Miranda","normal"));
		clientes.addElement(new Cliente(006,"Ricardo Quintero","gold"));
		
		videos.addElement(new Video(001,"El Padrino",30,"adultos"));
		videos.addElement(new Video(002,"Lord of the Rings",35,"adolecentes"));
		videos.addElement(new Video(003,"Alien",25,"adultos"));
		videos.addElement(new Video(004,"Toy Story",20,"infantil"));
		videos.addElement(new Video(005,"Star Wars",25,"adolecentes"));
		videos.addElement(new Video(006,"2001",32,"Adultos"));
		videos.addElement(new Video(007,"Pulp Fiction",28,"Adultos"));
		
	}
	
	public void agregarRenta(Renta v){
		rentas.addElement(v);
	}
	public int totalPorCategoria(){
		int infantil = 0;
		int adolecentes = 0;
		int adultos = 0;
		for (int i=0; i<rentas.size();i++){
			for(int j= 0; j<rentas.elementAt(i).videos.size(); j++){
				if(rentas.elementAt(i).videos.elementAt(j).tipo == "infantil"){
					infantil+=rentas.elementAt(i).videos.elementAt(j).precio;
				}
				if(rentas.elementAt(i).videos.elementAt(j).tipo == "adolecentes"){
					adolecentes+=rentas.elementAt(i).videos.elementAt(j).precio;
				}
				if(rentas.elementAt(i).videos.elementAt(j).tipo == "adultos"){
					adultos+=rentas.elementAt(i).videos.elementAt(j).precio;
				}
			}
			
		}
		System.out.println("El total por categoria infantil es $" + infantil);
		System.out.println("El total por categoria adolecentes es $" + adolecentes);
		System.out.println("El total por categoria adultos es $" + adultos);
		System.out.println(" ");
		
		return infantil;
	}
	public Vector<Video> getVideos(){
		return videos;
		
	}
	
	public boolean existeCliente(int id){
		for (int i=0; i<clientes.size();i++){
			if(clientes.elementAt(i).getCodigo_Cliente()==id){
				return true;
			}
			
		}
		return false;
	}
	
	public boolean existeVideo(int id){
		for (int i=0; i<videos.size();i++){
			if(videos.elementAt(i).getId()==id){
				return true;
			}			
		}
		return false;
	}
	
	public Video buscarVideo(int id){
		for (int i=0; i<videos.size();i++){
			if(videos.elementAt(i).getId()==id){
				return videos.elementAt(i);
			}			
		}
		return null;
	}
	
	public boolean existeRenta(int id){
		for (int i=0; i<rentas.size();i++){
			if(rentas.elementAt(i).codigo_cliente == id){
				return true;
			}			
		}
		return false;
	}
	
	public void totalPagarMembrecia(int id){
		float subtotal = 0;
		for (int i=0; i<rentas.size();i++){
			if(rentas.elementAt(i).codigo_cliente == id){
				for (int j=0; j<clientes.size(); j++){
					if(rentas.elementAt(i).codigo_cliente == clientes.elementAt(j).codigo_cliente){
						if(clientes.elementAt(j).membresia=="gold"){
							for(int k=0;k<rentas.elementAt(i).videos.size(); k++){
								subtotal+=rentas.elementAt(i).videos.elementAt(k).precio;
							}
							float total = (float) (subtotal - (subtotal*0.30));
							System.out.println("Cliente con membrecia gold se aplica 30 %, total $"+total);
						}else{
							for(int k=0;k<rentas.elementAt(i).videos.size(); k++){
								subtotal+=rentas.elementAt(i).videos.elementAt(k).precio;
							}
							System.out.println("Cliente con membrecia normal, total $"+subtotal);
						}
						
						
					}
				}
			}
		}
		
		
		
	}
}
