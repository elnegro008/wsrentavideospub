package renta;

import java.util.Vector;

public class Renta {
	int codigo_cliente;
	Vector<Video> videos;
	
	public Renta(int c, Vector<Video> v){
		codigo_cliente=c;
		videos=v;
	}
	
	
	public int getCodigo_Cliente(){
		return codigo_cliente;
	}
	
	public Vector<Video> getVideos(){
		return videos;
	}
	
	public int sumaTotal(){
		int total=0;
		for(int i=0;i<videos.size();i++){
			total=total+videos.elementAt(i).getPrecio();			
		}
		return total;
	}
	
	
	

}
