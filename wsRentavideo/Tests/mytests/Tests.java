package mytests;

import static org.junit.Assert.*;

import java.util.Vector;

import org.junit.Test;

import renta.Datos;
import renta.Renta;
import renta.Video;

public class Tests {
	Vector<Video> videos = new Vector<Video>();
	Datos d;
	Renta r1;

	@Test
	public void test() {
		videos.add(new Video(001,"StarWars",30,"Adolecentes"));
		videos.add(new Video(003,"Toy Story",25,"Infantil"));
		r1=new Renta(005,videos);
		assertTrue(r1.sumaTotal()==55);
	}
	
	@Test
	public void testTotalPorCategoria() {
		
		videos.add(new Video(001,"StarWars",30,"Adolecentes"));
		videos.add(new Video(002,"Toy Story",25,"Infantil"));
		videos.add(new Video(003,"Iron Man",30,"Adolecentes"));
		videos.add(new Video(004,"Thor",25,"Infantil"));
		videos.add(new Video(005,"XXX",30,"Adultos"));
		videos.add(new Video(006,"Hulk",25,"Infantil"));
		r1=new Renta(005,videos);
		d = new Datos();
		d.agregarRenta(r1);
		assertTrue(d.totalPorCategoria()==75);
	}

}
